package cz.inited.naramekb;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.*;
import android.os.*;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static android.bluetooth.BluetoothGattCharacteristic.FORMAT_UINT8;

public class BackgroundService extends Service {


    public Context context = this;
    public Handler handler = null;
    public static Runnable runnable = null;

    private static int ONGOING_NOTIFICATION_ID = 1;


    public static final String NOTIFICATION_CHANNEL_ID_SERVICE = "com.mypackage.service";
    public static final String NOTIFICATION_CHANNEL_ID_INFO = "com.mypackage.download_info";

    private final String BATTERY_SERVICE = "0000180f-0000-1000-8000-00805f9b34fb";
    private final String BATTERY_CHARACTERISTIC = "00002a19-0000-1000-8000-00805f9b34fb";
    private final String BATTERY_DESCRIPTOR = "00002902-0000-1000-8000-00805f9b34fb";

    private BluetoothAdapter bluetoothAdapter;
    BluetoothGatt bluetoothGatt;
    RequestQueue httpQueue;


    private boolean workEnabled = false;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, "Service created!", Toast.LENGTH_LONG).show();

        handler = new Handler();
        runnable = new Runnable() {
            public void run() {
                Toast.makeText(context, "Service is still running", Toast.LENGTH_LONG).show();
                handler.postDelayed(runnable, 10000);
            }
        };

        handler.postDelayed(runnable, 15000);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nm.createNotificationChannel(new NotificationChannel(NOTIFICATION_CHANNEL_ID_SERVICE, "App Service", NotificationManager.IMPORTANCE_DEFAULT));
            nm.createNotificationChannel(new NotificationChannel(NOTIFICATION_CHANNEL_ID_INFO, "Download Info", NotificationManager.IMPORTANCE_DEFAULT));
        }



        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification =
                new Notification.Builder(this, NOTIFICATION_CHANNEL_ID_INFO)
                        .setContentTitle("muj title")
                        .setContentText("muj text")
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentIntent(pendingIntent)
                        .setTicker("muj ticker")
                        .build();

        startForeground(ONGOING_NOTIFICATION_ID, notification);



    }


    @Override
    public void onDestroy() {
        workEnabled = false;
        handler.removeCallbacks(runnable);
        unsubscribe();
        disconnect();


        Toast.makeText(this, "Service stopped", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStart(Intent intent, int startid) {
        Toast.makeText(this, "Service started by user.", Toast.LENGTH_LONG).show();

        workEnabled = true;
        httpQueue = Volley.newRequestQueue(this);
        connect();
    }







    public void connect() {
        String address = "D6:8B:54:AA:70:13";
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


        //Ensure device exists
        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);

        boolean autoConnect = false;
        bluetoothGatt = device.connectGatt(getApplicationContext(), autoConnect, bluetoothGattCallback);

    }

    public void disconnect() {
        bluetoothGatt.disconnect();
    }

    //Bluetooth callback for connecting, discovering, reading and writing
    private BluetoothGattCallback bluetoothGattCallback = new BluetoothGattCallback() {
        @Override
        public void onPhyUpdate(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
            super.onPhyUpdate(gatt, txPhy, rxPhy, status);
            Log.d("BLE", "BT onPhyUpdate");
        }

        @Override
        public void onPhyRead(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
            super.onPhyRead(gatt, txPhy, rxPhy, status);
            Log.d("BLE", "BT onPhyRead");
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            Log.d("BLE", "BT onConnectionStateChange: " + status + " : " + newState);


            //Device was connected
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                discover();
            }

            //Device was disconnected
            if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                if (workEnabled) {
                    connect();
                }
            }


        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            Log.d("BLE", "BT onServicesDiscovered");

            //Get the connected device
            BluetoothDevice device = gatt.getDevice();
            String address = device.getAddress();

            //If successfully discovered, return list of services, characteristics and descriptors
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d("BLE", "BT onServicesDiscovered - dopadlo to dobre");

                List<BluetoothGattDescriptor> myDescriptors = new ArrayList<>();

                List<BluetoothGattService> services = bluetoothGatt.getServices();

                for (BluetoothGattService service : services) {

                    Log.d("BLE", "BT onServicesDiscovered - service: " + service.getUuid());


                    List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();

                    for (BluetoothGattCharacteristic characteristic : characteristics) {
                        Log.d("BLE", "BT onServicesDiscovered - characteristic: " + characteristic.getUuid());

                        List<BluetoothGattDescriptor> descriptors = characteristic.getDescriptors();

                        for (BluetoothGattDescriptor descriptor : descriptors) {


                            Log.d("BLE", "BT onServicesDiscovered - descriptor: " + descriptor.getUuid());

                            myDescriptors.add(descriptor);

                        }
                    }
                }

                subscribe();

            } else {
                Log.d("BLE", "BT onServicesDiscovered - dopadlo to spatne");
            }


        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            Log.d("BLE", "BT onCharacteristicRead");
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            Log.d("BLE", "BT onCharacteristicWrite");
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            Log.d("BLE", "BT onCharacteristicChanged:" + characteristic.getIntValue(FORMAT_UINT8, 0));

            httpCall("https://work.pavrda.cz/?baterka=" + characteristic.getIntValue(FORMAT_UINT8, 0));

            bluetoothGatt.readRemoteRssi();
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
            Log.d("BLE", "BT onDescriptorRead");
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            Log.d("BLE", "BT onDescriptorWrite");
        }

        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            super.onReliableWriteCompleted(gatt, status);
            Log.d("BLE", "BT onReliableWriteCompleted");
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
            Log.d("BLE", "BT onReadRemoteRssi");
            if(status == BluetoothGatt.GATT_SUCCESS) {
                Log.d("BLE", String.format("BluetoothGat ReadRssi[%d]", rssi));
                httpCall("https://work.pavrda.cz/?rssi=" + rssi);
            }
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);
            Log.d("BLE", "BT onMtuChanged");
        }
    };


    private void discover() {
        bluetoothGatt.discoverServices();

    }

    private void subscribe() {

        BluetoothGattService service = bluetoothGatt.getService(UUID.fromString(BATTERY_SERVICE));
        BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(BATTERY_CHARACTERISTIC));
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString(BATTERY_DESCRIPTOR));


        if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) == BluetoothGattCharacteristic.PROPERTY_NOTIFY) {
            Log.d("BLE", "BT subscribe - notifiactions");
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        } else {
            Log.d("BLE", "BT subscribe - indications");
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
        }

        bluetoothGatt.writeDescriptor(descriptor);
        bluetoothGatt.setCharacteristicNotification(characteristic, true);

        Log.d("BLE", "BT subscribed");

    }

    private void unsubscribe() {

        BluetoothGattService service = bluetoothGatt.getService(UUID.fromString(BATTERY_SERVICE));
        BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(BATTERY_CHARACTERISTIC));
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString(BATTERY_DESCRIPTOR));


        if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) == BluetoothGattCharacteristic.PROPERTY_NOTIFY) {
            Log.d("BLE", "BT subscribe - notifiactions");
            descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
        } else {
            Log.d("BLE", "BT subscribe - indications");
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
        }

        bluetoothGatt.writeDescriptor(descriptor);
        bluetoothGatt.setCharacteristicNotification(characteristic, false);

        Log.d("BLE", "BT unsubscribed");

    }




    public void httpCall(String url) {

        sendMessageToActivity(url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("BLE", "BT http response:" + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BLE", "BT http error:" + error);
            }
        });

        httpQueue.add(stringRequest);
    }


    private void sendMessageToActivity(String msg) {
        Intent intent = new Intent("NaramekUpdates");
        // You can also include some extra data.
        intent.putExtra("Status", msg);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

}
